import pandas as pd
import numpy as np
import matplotlib.pyplot as plt 
import matplotlib.image as mpimg
import seaborn as sns
import pytz
import re
import nltk

import tensorflow as tf
import os
import pickle

from tensorflow import keras
from tensorflow.keras.utils import plot_model, to_categorical

from matplotlib import cm

from datetime import datetime
from sklearn.preprocessing import OneHotEncoder,LabelEncoder
from sklearn.metrics import classification_report, confusion_matrix, log_loss
from sklearn.model_selection import train_test_split
from sklearn.utils import resample

from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer

class F1Score(tf.keras.metrics.Metric):
    def __init__(self, name='f1_score', **kwargs):
        super(F1Score, self).__init__(name=name, **kwargs)
        self.true_positives = self.add_weight(name='tp', initializer='zeros')
        self.false_positives = self.add_weight(name='fp', initializer='zeros')
        self.false_negatives = self.add_weight(name='fn', initializer='zeros')

    def update_state(self, y_true, y_pred, sample_weight=None):
        y_true = tf.argmax(y_true, axis=1)
        y_pred = tf.argmax(y_pred, axis=1)
        true_positives = tf.reduce_sum(tf.cast(tf.logical_and(tf.equal(y_true, 1), tf.equal(y_pred, 1)), tf.float32))
        false_positives = tf.reduce_sum(tf.cast(tf.logical_and(tf.equal(y_true, 0), tf.equal(y_pred, 1)), tf.float32))
        false_negatives = tf.reduce_sum(tf.cast(tf.logical_and(tf.equal(y_true, 1), tf.equal(y_pred, 0)), tf.float32))
        self.true_positives.assign_add(true_positives)
        self.false_positives.assign_add(false_positives)
        self.false_negatives.assign_add(false_negatives)

    def result(self):
        precision = self.true_positives / (self.true_positives + self.false_positives + tf.keras.backend.epsilon())
        recall = self.true_positives / (self.true_positives + self.false_negatives + tf.keras.backend.epsilon())
        f1 = 2 * precision * recall / (precision + recall + tf.keras.backend.epsilon())
        return f1

def get_unique_tweet_created_days(df):
    unique_days = pd.to_datetime(df['tweet_created']).dt.date.unique()
    sorted_days = sorted(unique_days, key=lambda x: pd.to_datetime(x))
    unique_days_str = [day.strftime("%d.%m.%Y") for day in sorted_days]
    
    return unique_days_str

def get_unique_as_string(df, feature):
    unique = sorted(df[feature].unique())
    return unique

def parse_time(time_range):
        start_str, end_str = time_range.split('-')
        start_time = datetime.strptime(start_str, '%H:%M').time()
        end_time = datetime.strptime(end_str, '%H:%M').time()
        return start_time, end_time

def sort_by_unique(unique, counts):
    unique = unique.numpy()
    counts = counts.numpy()
    sorted_indices = np.argsort(unique, axis=-1)
    sorted_unique = unique[sorted_indices]
    sorted_counts = counts[sorted_indices]
    return sorted_unique, sorted_counts

def encode_time_to_1h_period(data, drop_old_feature=True):

    data['tweet_hour'] = data['tweet_created'].dt.hour

    if drop_old_feature == True:
        data = data.drop(columns = 'tweet_created')
    return data

def encode_time_to_3h_period(data, drop_old_feature=True):
    time_intervals = [(0, 2), (3, 5), (6, 8), (9, 11), (12, 14), (15, 17), (18, 20), (21, 23)]
    time_labels = ['0:00-2:59', '3:00-5:59', '6:00-8:59', '9:00-11:59', '12:00-14:59', '15:00-17:59', '18:00-20:59', '21:00-23:59']

    data['tweet_hour'] = data['tweet_created'].dt.hour
    data['tweet_3hour'] = pd.cut(data['tweet_hour'], bins=[interval[0] for interval in time_intervals] + [time_intervals[-1][1]+1], labels=time_labels, right=False, include_lowest=True)

    data.drop('tweet_hour', axis=1, inplace=True)

    if drop_old_feature == True:
        data = data.drop(columns = 'tweet_created')
    return data

def encode_feature(data, feature, drop_old_feature=True, return_only_encoded=False):
    new_features = sorted(data[feature].unique())
    encoder = OneHotEncoder(handle_unknown='ignore')
    encoder_df = pd.DataFrame(encoder.fit_transform(data[[feature]]).toarray())
    for index, new_feature in enumerate(new_features):
        encoder_df=encoder_df.rename(columns={index : new_feature})
    cumulative_df = data.join(encoder_df)
    if drop_old_feature == True:
        cumulative_df = cumulative_df.drop(columns = feature)
    if return_only_encoded == False:
        return cumulative_df
    else:
        return cumulative_df[new_features]


def groupbyTime(data, percent=False, unit='hour'):
    data['tweet_created'] = pd.to_datetime(data['tweet_created'])
    if unit == 'hour':
        time_counted = data.groupby(data['tweet_created'].dt.hour).count()
    elif unit == 'day':
        time_counted = data.groupby(data['tweet_created'].dt.day).count()
    if percent:
        time_counted = time_counted / data.count() * 100
    indices = time_counted.index
    counts = time_counted['tweet_id']
    return indices, counts

def get_timezone(city):
    if isinstance(city, pytz.tzinfo.BaseTzInfo):
        return city
    try:
        timezone = pytz.timezone(city)
        return timezone
    except pytz.UnknownTimeZoneError:
        return None
    
def decode_label(y, class_labels):
    y = np.argmax(y, axis=1)
    y = [class_labels[i] for i in y]
    return y

def remove_emojis(text):
    emoji_pattern = re.compile("["
        u"\U0001F600-\U0001F64F"  
        u"\U0001F300-\U0001F5FF"  
        u"\U0001F680-\U0001F6FF"  
        u"\U0001F1E0-\U0001F1FF"  
                           "]+", flags = re.UNICODE)
    return emoji_pattern.sub(r'',text)

def is_emoji(char):
    emoji_pattern = re.compile("["
        u"\U0001F600-\U0001F64F"  
        u"\U0001F300-\U0001F5FF"  
        u"\U0001F680-\U0001F6FF"  
        u"\U0001F1E0-\U0001F1FF"  
                           "]+", flags = re.UNICODE)
    return bool(emoji_pattern.match(char))

def remove_special_characters(sentence):
    cleaned_sentence = ''.join(char for char in sentence if char.isalnum()  or char.isspace() or is_emoji(char))
    cleaned_sentence = re.sub(r'\s+', ' ', cleaned_sentence)
    return cleaned_sentence

def remove_num_characters(sentence):
    cleaned_sentence = ''.join(char for char in sentence if not(char.isnumeric()))
    cleaned_sentence = re.sub(r'\s+', ' ', cleaned_sentence)
    return cleaned_sentence


def remove_mention(text):
    text_without_mention = re.sub(r'@\w+', '', text)
    return text_without_mention

def add_space(text):
    modified_text = ""
    for char in text:
        if is_emoji(char):
            modified_text += " " + char + " "
        else:
            modified_text += char
    modified_text = re.sub(r"\s+", " ", modified_text).strip()
    return modified_text

nltk.download('stopwords')
stop_words = set(stopwords.words('english'))

def remove_stopwords(text):
    tokens = text.split()
    filtered_tokens = [word for word in tokens if word.lower() not in stop_words]
    return ' '.join(filtered_tokens)

nltk.download('averaged_perceptron_tagger')
nltk.download('wordnet')
lemmatizer = WordNetLemmatizer()

def lemmatize_sentence_with_pos(sentence):
    tokens = nltk.word_tokenize(sentence)
    pos_tags = nltk.pos_tag(tokens)
    
    lemmatized_tokens = []
    for token, pos_tag in pos_tags:
        if pos_tag.startswith('N'):
            lemmatized_token = lemmatizer.lemmatize(token, pos='n')
        elif pos_tag.startswith('V'):
            lemmatized_token = lemmatizer.lemmatize(token, pos='v')
        elif pos_tag.startswith('J'):
            lemmatized_token = lemmatizer.lemmatize(token, pos='a')
        elif pos_tag.startswith('R'):
            lemmatized_token = lemmatizer.lemmatize(token, pos='r')
        else:
            lemmatized_token = lemmatizer.lemmatize(token)
        lemmatized_tokens.append(lemmatized_token)
    
    lemmatized_sentence = ' '.join(lemmatized_tokens)
    return lemmatized_sentence

def sample_down(data, random_state):
    negative = data[data['airline_sentiment'] == 'negative']
    neutral = data[data['airline_sentiment'] == 'neutral']
    positive = data[data['airline_sentiment'] == 'positive']

    downsampled_negative = resample(negative, replace=False, n_samples=len(positive), random_state=random_state)
    downsampled_neutral = resample(neutral, replace=False, n_samples=len(positive), random_state=random_state)

    data_downsampled = pd.concat([downsampled_negative, downsampled_neutral, positive])
    return data_downsampled

def split_train_val_test_split(X, y, dev_size, test_size, random_state, shuffle=True):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=random_state,shuffle=True)
    X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=np.round(dev_size/(1-test_size),decimals = 10), random_state=random_state,shuffle=False)
    return X_train, X_val, X_test, y_train, y_val, y_test

def save_model(model, model_name):
    if not os.path.exists('models'):
        os.makedirs('models')
    file_name = model_name + ".h5"
    file_path = os.path.join('models', file_name)
    model.save(file_path)

def load_model(model_name):
    file_name = model_name + ".h5"
    file_path = os.path.join('models', file_name)
    model = keras.models.load_model(file_path)
    return model

def save_hyperparameter(model_name, learning_rate_hyperparam, num_epochs, use_momentum, momentum_hyperparam, mini_batch_size, do_sample_weights, use_early_stopping, used_optimizer, total_params, trainable_params, non_trainable_params):
    hyperparameters = f'''
    Learning_rate: {learning_rate_hyperparam}
    Epochs: {num_epochs}
    Is Momentum Used?: {use_momentum}
    Beta (Momentum): {momentum_hyperparam}
    Mini_batch_size: {mini_batch_size}
    do_sample_weights: {do_sample_weights}
    use_early_stopping: {use_early_stopping}
    used_optimizer: {used_optimizer}
    Total params: {total_params}
    Trainable params: {trainable_params}
    Non-trainable params: {non_trainable_params}'''

    hyperparameters = hyperparameters.strip()
    hyperparameters = hyperparameters.replace('    ', '')

    if not os.path.exists('hyperparameter'):
        os.makedirs('hyperparameter')
    file_name= model_name + ".txt"
    file_path = os.path.join('hyperparameter', file_name)
    with open(file_path, 'wb') as file:
        pickle.dump(hyperparameters, file)

def load_hyperparameter(model_name):
    file_name= model_name + ".txt"
    file_path = os.path.join('hyperparameter', file_name)
    with open(file_path, 'rb') as file:
        hyperparameters = pickle.load(file)
    return hyperparameters

def save_history(history, model_name):
    if not os.path.exists('histories'):
        os.makedirs('histories')
    file_name = model_name + ".pkl"
    file_path = os.path.join('histories', file_name)
    with open(file_path, 'wb') as file:
        pickle.dump(history, file)

def load_history(model_name):
    file_name = model_name + ".pkl"
    file_path = os.path.join('histories', file_name)
    with open(file_path, 'rb') as file:
        history = pickle.load(file)
    return history

def save_classifier(classifier_name, classifier):
    if not os.path.exists('classifiers'):
        os.makedirs('classifiers')
    file_name= classifier_name + ".pkl"
    file_path = os.path.join('classifiers', file_name)
    pickle.dump(classifier, open(file_path, 'wb'))

def load_classifier(classifier_name):
    file_name= classifier_name + ".pkl"
    file_path = os.path.join('classifiers', file_name)
    loaded_classifier = pickle.load(open(file_path, 'rb'))
    return loaded_classifier

def extract_model_params(model):
    stringlist = []
    model.summary(print_fn=lambda x: stringlist.append(x))
    summary_output = "\n".join(stringlist)

    def extract_value(line):
        start = line.find(':') + 1
        end = line.find('\n')
        value = line[start:end].strip().replace(',', '')
        return int(value)

    total_params_line = summary_output[summary_output.find('Total params'):summary_output.find('Trainable params')]
    trainable_params_line = summary_output[summary_output.find('Trainable params'):summary_output.find('Non-trainable params')]
    non_trainable_params_line = summary_output[summary_output.find('Non-trainable params'):]

    total_params = extract_value(total_params_line)
    trainable_params = extract_value(trainable_params_line)
    non_trainable_params = extract_value(non_trainable_params_line)

    return total_params, trainable_params, non_trainable_params

def plot_metrics_for_models():
    folder_path = 'histories'
    file_names = os.listdir(folder_path)

    for file_name in file_names:
        model_name = os.path.splitext(file_name)[0]
        history = load_history(model_name)
        model = load_model(model_name)
        
        print(history)
        plot_metrics(model, history, model_name)

def plot_metrics(model, history, model_name, show_in_row=False):

    if show_in_row == True:
        fig, axes = plt.subplots(1, 6, figsize=(24, 4))
    else:
        fig, axes = plt.subplots(1, 7, figsize=(20, 8))
        axes[0] = plt.subplot2grid(shape=(2, 4), loc=(0, 0), colspan=1)
        axes[1] = plt.subplot2grid(shape=(2, 4), loc=(0, 1), colspan=1)
        axes[2] = plt.subplot2grid(shape=(2, 4), loc=(0, 2), colspan=1)
        axes[3] = plt.subplot2grid(shape=(2, 4), loc=(1, 0), colspan=1)
        axes[4] = plt.subplot2grid(shape=(2, 4), loc=(1, 1), colspan=1)
        axes[5] = plt.subplot2grid(shape=(2, 4), loc=(1, 2), colspan=1)
        axes[6] = plt.subplot2grid(shape=(2, 4), loc=(0, 3), rowspan=2)

    fig.suptitle(model_name, fontsize=16)
    
    cmap = plt.get_cmap('viridis')
    num_epochs = len(history.history['loss'])
    val_color = cmap(0.8)
    train_color = cmap(0.2)
    
    # Loss
    axes[0].plot(history.history['loss'], label='Train Loss', color=train_color)
    axes[0].plot(history.history['val_loss'], label='Validation Loss', color=val_color)
    axes[0].set_title('Loss')
    axes[0].set_xlabel('Epoch')
    axes[0].set_ylabel('Loss Value')
    axes[0].legend()
    
    # Accuracy
    axes[1].plot(history.history['accuracy'], label='Train Accuracy', color=train_color)
    axes[1].plot(history.history['val_accuracy'], label='Validation Accuracy', color=val_color)
    axes[1].set_title('Accuracy')
    axes[1].set_xlabel('Epoch')
    axes[1].set_ylabel('Accuracy Value')
    axes[1].legend()
    
    # Precision
    axes[2].plot(history.history['precision'], label='Train Precision', color=train_color)
    axes[2].plot(history.history['val_precision'], label='Validation Precision', color=val_color)
    axes[2].set_title('Precision')
    axes[2].set_xlabel('Epoch')
    axes[2].set_ylabel('Precision Value')
    axes[2].legend()
    
    # Recall
    axes[3].plot(history.history['recall'], label='Train Recall', color=train_color)
    axes[3].plot(history.history['val_recall'], label='Validation Recall', color=val_color)
    axes[3].set_title('Recall')
    axes[3].set_xlabel('Epoch')
    axes[3].set_ylabel('Recall Value')
    axes[3].legend()

    # F1-Score
    f1_train = history.history['f1_score']
    f1_val = history.history['val_f1_score']
    axes[4].plot(f1_train, label='Train F1-Score', color=train_color)
    axes[4].plot(f1_val, label='Validation F1-Score', color=val_color)
    axes[4].set_title('F1-Score')
    axes[4].set_xlabel('Epoch')
    axes[4].set_ylabel('F1-Score Value')
    axes[4].legend()

    axes[5].axis('off')
    string = load_hyperparameter(model_name)
    lines = string.split('\n')
    data = [line.split(': ') for line in lines if line]

    rgb_value = (0x25 / 255, 0x25 / 255, 0x26 / 255)
    colors = [[rgb_value] * len(data[0])] * len(data)
    table = axes[5].table(cellText=data, loc='center', cellLoc='left', cellColours=colors)

    for _, cell in table.get_celld().items():
        cell.set_edgecolor('white')

    table.auto_set_font_size(False)
    table.set_fontsize(12)
    table.scale(1, 2)
    
    plot_model(model, to_file='temp/model_architecture_temp.png', show_shapes=True)
    image = mpimg.imread('temp/model_architecture_temp.png')
    axes[6].imshow(image)
    axes[6].axis('off')

    plt.tight_layout()
    plt.show()

def extract_metrics(report, metric):
    if isinstance(report, dict):
        if metric == 'support':
            return [report[label][metric] for label in report.keys()]
        else:
            return [report[label][metric] for label in report.keys() if label != 'accuracy']
    elif isinstance(report, float):
        return report
    else:
        return None

def plot_classification_metrics(report):
    class_metrics = report.copy()
    class_metrics.pop('accuracy', None)
    class_metrics.pop('macro avg', None)
    class_metrics.pop('weighted avg', None)
    
    classes = list(class_metrics.keys())
    indices = range(len(classes))
    
    precision = extract_metrics(class_metrics, 'precision')
    recall = extract_metrics(class_metrics, 'recall')
    f1_score = extract_metrics(class_metrics, 'f1-score')
    
    plt.figure(figsize=(8, 4))
    
    plt.subplot(1, 3, 1)
    plt.bar(indices, precision, color='blue')
    plt.title('Precision')
    plt.xlabel('Sentiment')
    plt.ylabel('Metrikwert')
    plt.xticks(indices, classes, rotation=45)
    
    plt.subplot(1, 3, 2)
    plt.bar(indices, recall, color='green')
    plt.title('Recall')
    plt.xlabel('Sentiment')
    plt.ylabel('Metrikwert')
    plt.xticks(indices, classes, rotation=45)
    
    plt.subplot(1, 3, 3)
    plt.bar(indices, f1_score, color='orange')
    plt.title('F1-Score')
    plt.xlabel('Sentiment')
    plt.ylabel('Metrikwert')
    plt.xticks(indices, classes, rotation=45)
    
    plt.tight_layout()
    plt.show()

def evaluate_nn_classifier(model, X_test, y_test, do_print=False):
    y_pred = model.predict(X_test, verbose=0)
    y_pred = np.argmax(y_pred, axis=1)
    y_true = np.argmax(y_test, axis=1)

    target_names = ['negative', 'neutral', 'positive']
    report = classification_report(y_true, y_pred, target_names=target_names, output_dict=True)
    if do_print == True:
        print('Neuronal Network')
        print(classification_report(y_true, y_pred, target_names=target_names))
    return report

def get_metric_table_row(classifier, classifier_name, X, y, nn=False, hyperparameters=None):
    
    if nn == True:
        report = evaluate_nn_classifier(classifier, X, y)
    else:
        predictions = classifier.predict(X)
        report = classification_report(y, predictions, output_dict=True)
        
    accuracy = report['accuracy']

    if nn == True:
        loss = classifier.evaluate(X, y, verbose=0)[0]
    else:
        loss = get_loss_of_classifier(classifier, X, y)
    
    metrics = ['Recall', 'Precision', 'F1-Score']
    sentiments = ['weighted avg', 'negative', 'neutral', 'positive']

    data = pd.DataFrame(columns=pd.MultiIndex.from_product([metrics, sentiments]))

    data['Modellname'] = classifier_name

    for sentiment in sentiments:
        sentiment_precision = report[sentiment]['precision']
        data.loc[0, ('Precision', sentiment)] = sentiment_precision
    for sentiment in sentiments:    
        sentiment_recall = report[sentiment]['recall']
        data.loc[0, ('Recall', sentiment)] = sentiment_recall
    for sentiment in sentiments:
        sentiment_f1_score = report[sentiment]['f1-score']
        data.loc[0, ('F1-Score', sentiment)] = sentiment_f1_score

    data.loc[0, 'Modellname'] = classifier_name
    data.loc[0, 'Accuracy'] = accuracy
    data.loc[0, 'Loss'] = loss

    data.set_index('Modellname', inplace=True)
    return data

def get_metric_table(X_classic, y_classic, X_nn, y_nn):
    nb_classifier = load_classifier('nb_classifier')
    dt_classifier = load_classifier('dt_classifier')
    rf_classifier = load_classifier('rf_classifier')

    metric_table = pd.DataFrame()
    metric_table = metric_table.append(get_metric_table_row(nb_classifier, 'Naive Bayes', X_classic, y_classic))
    metric_table = metric_table.append(get_metric_table_row(dt_classifier, 'Decision Tree', X_classic, y_classic))
    metric_table = metric_table.append(get_metric_table_row(rf_classifier, 'Random Forest', X_classic, y_classic))

    folder_path = 'models'
    file_names = os.listdir(folder_path)

    for file_name in file_names:
        model_name = os.path.splitext(file_name)[0]
        hyperparameters = load_hyperparameter(model_name)
        model = load_model(model_name)
    
        metric_table = metric_table.append(get_metric_table_row(model, model_name + ' (NN)', X_nn, y_nn, nn=True, hyperparameters=hyperparameters))

    metric_table = metric_table.fillna('')
    return metric_table

def highlight_metric_table(metric_table):
    return metric_table.style.highlight_max(color='gray', subset=['Recall', 'Precision', 'F1-Score', 'Accuracy'])

def get_loss_of_classifier(classifier, X, y):
    predictions = classifier.predict(X)

    label_encoder = LabelEncoder()
    label_encoder.fit(y)

    y_encoded = label_encoder.transform(y)
    predictions_encoded = label_encoder.transform(predictions)

    y_encoded_one_hot = to_categorical(y_encoded)
    predictions_encoded_one_hot = to_categorical(predictions_encoded)
    loss = log_loss(y_encoded_one_hot, predictions_encoded_one_hot)
    return loss

def plot_metrics_from_metric_table(metric_table):
    fig, axes = plt.subplots(1, 8, figsize=(24, 4))

    cmap = cm.get_cmap("viridis")
    colors = [cmap(i) for i in np.linspace(0, 255, metric_table.shape[0]).astype(int)]

    # Precision weighted avg
    axes[0].bar(metric_table.index, metric_table[('Precision', 'weighted avg')], color=colors)
    axes[0].set_title('Precision weighted avg')
    axes[0].set_xlabel('Classifier')
    axes[0].set_ylabel('Value')
    axes[0].set_xticklabels(axes[0].get_xticklabels(), rotation=90)

    # Precision negative
    axes[1].bar(metric_table.index, metric_table[('Precision', 'negative')], color=colors)
    axes[1].set_title('Precision negative')
    axes[1].set_xlabel('Classifier')
    axes[1].set_ylabel('Value')
    axes[1].set_xticklabels(axes[0].get_xticklabels(), rotation=90)

    # Recall weighted avg
    axes[2].bar(metric_table.index, metric_table[('Recall', 'weighted avg')], color=colors)
    axes[2].set_title('Recall weighted avg')
    axes[2].set_xlabel('Classifier')
    axes[2].set_ylabel('Value')
    axes[2].set_xticklabels(axes[1].get_xticklabels(), rotation=90)

    # Recall negative
    axes[3].bar(metric_table.index, metric_table[('Recall', 'negative')], color=colors)
    axes[3].set_title('Recall negative')
    axes[3].set_xlabel('Classifier')
    axes[3].set_ylabel('Value')
    axes[3].set_xticklabels(axes[1].get_xticklabels(), rotation=90)

    # F1-Score weighted avg
    axes[4].bar(metric_table.index, metric_table[('F1-Score', 'weighted avg')], color=colors)
    axes[4].set_title('F1-Score weighted avg')
    axes[4].set_xlabel('Classifier')
    axes[4].set_ylabel('Value')
    axes[4].set_xticklabels(axes[2].get_xticklabels(), rotation=90)

    # F1-Score negative
    axes[5].bar(metric_table.index, metric_table[('F1-Score', 'negative')], color=colors)
    axes[5].set_title('F1-Score negative')
    axes[5].set_xlabel('Classifier')
    axes[5].set_ylabel('Value')
    axes[5].set_xticklabels(axes[2].get_xticklabels(), rotation=90)

    # Accuracy
    axes[6].bar(metric_table.index, metric_table['Accuracy'], color=colors)
    axes[6].set_title('Accuracy')
    axes[6].set_xlabel('Classifier')
    axes[6].set_ylabel('Value')
    axes[6].set_xticklabels(axes[3].get_xticklabels(), rotation=90)

    # Loss
    axes[7].bar(metric_table.index, metric_table['Loss'], color=colors)
    axes[7].set_title('Loss')
    axes[7].set_xlabel('Classifier')
    axes[7].set_ylabel('Value')
    axes[7].set_xticklabels(axes[3].get_xticklabels(), rotation=90)

    plt.tight_layout()
    plt.show()

def get_confusion_matrix_table_row(classifier, classifier_name, X, y, nn=False):

    if nn == False:
        y_pred = classifier.predict(X)
        cm_absolut = confusion_matrix(y, y_pred)
    else:
        y_pred = classifier.predict(X, verbose=0)
        cm_absolut = confusion_matrix(np.argmax(y, axis=1), np.argmax(y_pred, axis=1))  
    
    cm_percent = cm_absolut / cm_absolut.sum(axis=1, keepdims=True) * 100

    metrics = ['True negative', 'True neutral', 'True positive']
    sentiments = ['Predicted negative', 'Predicted neutral', 'Predicted positive']

    data = pd.DataFrame(columns=pd.MultiIndex.from_product([metrics, sentiments]))

    data.loc[0, ('True negative', 'Predicted negative')] = f'{round(cm_percent[0,0], 1)} % ({cm_absolut[0,0]})'
    data.loc[0, ('True negative', 'Predicted neutral')] =  f'{round(cm_percent[0,1], 1)} % ({cm_absolut[0,1]})'
    data.loc[0, ('True negative', 'Predicted positive')] = f'{round(cm_percent[0,2], 1)} % ({cm_absolut[0,2]})'
    data.loc[0, ('True neutral', 'Predicted negative')] =  f'{round(cm_percent[1,0], 1)} % ({cm_absolut[1,0]})'
    data.loc[0, ('True neutral', 'Predicted neutral')] =   f'{round(cm_percent[1,1], 1)} % ({cm_absolut[1,1]})'
    data.loc[0, ('True neutral', 'Predicted positive')] =  f'{round(cm_percent[1,2], 1)} % ({cm_absolut[1,2]})'
    data.loc[0, ('True positive', 'Predicted negative')] = f'{round(cm_percent[2,0], 1)} % ({cm_absolut[2,0]})'
    data.loc[0, ('True positive', 'Predicted neutral')] =  f'{round(cm_percent[2,1], 1)} % ({cm_absolut[2,1]})'
    data.loc[0, ('True positive', 'Predicted positive')] = f'{round(cm_percent[2,2], 1)} % ({cm_absolut[2,2]})'
 
    data['Sortout non negatives'] = f'{round((cm_absolut[1,0] + cm_absolut[2,0]) / (cm_absolut[0,0] + cm_absolut[1,0] + cm_absolut[2,0]) * 100, 1)} %'

    data['Modellname'] = classifier_name
    data.set_index('Modellname', inplace=True)

    return data

def get_confusion_matrix_table(X_classic, y_classic, X_nn, y_nn):

    confusion_matrix_table = pd.DataFrame()
    
    nb_classifier = load_classifier('nb_classifier')
    dt_classifier = load_classifier('dt_classifier')
    rf_classifier = load_classifier('rf_classifier')

    confusion_matrix_table = confusion_matrix_table.append(get_confusion_matrix_table_row(nb_classifier, 'Naive Bayes', X_classic, y_classic, nn=False))
    confusion_matrix_table = confusion_matrix_table.append(get_confusion_matrix_table_row(dt_classifier, 'Decision Tree', X_classic, y_classic, nn=False))
    confusion_matrix_table = confusion_matrix_table.append(get_confusion_matrix_table_row(rf_classifier, 'Random Forest', X_classic, y_classic, nn=False))

    folder_path = 'models'
    file_names = os.listdir(folder_path)
    for file_name in file_names:
        model_name = os.path.splitext(file_name)[0]
        model = load_model(model_name)
        confusion_matrix_table = confusion_matrix_table.append(get_confusion_matrix_table_row(model, model_name + ' (NN)', X_nn, y_nn, nn=True))
    
    return confusion_matrix_table

def plot_confusion_matrices(X_classic, y_classic, X_nn, y_nn):
    classifiers = []
    classifiers.append(load_classifier('nb_classifier'))
    classifiers.append(load_classifier('dt_classifier'))
    classifiers.append(load_classifier('rf_classifier'))

    models = []
    model_names = []
    folder_path = 'models'
    file_names = os.listdir(folder_path)
    for file_name in file_names:
        model_name = os.path.splitext(file_name)[0]
        model_names.append(model_name)
        models.append(load_model(model_name))

    models = models[:3]

    class_names = ['negative', 'neutral', 'positive']

    fig, axes = plt.subplots(2, 3, figsize=(9, 6))

    for i, classifier in enumerate(classifiers):
        y_pred = classifier.predict(X_classic)
        cm_absolut = confusion_matrix(y_classic, y_pred)
        cm_percent = cm_absolut / cm_absolut.sum(axis=1, keepdims=True) * 100
        
        ax = axes[0, i]
        ax.imshow(cm_percent, interpolation='nearest', cmap='viridis')
        ax.set_title(classifier.__class__.__name__)
        ax.set_xticks(np.arange(len(class_names)))
        ax.set_yticks(np.arange(len(class_names)))
        ax.set_xticklabels(class_names)
        ax.set_yticklabels(class_names)
        ax.set_xlabel('Predicted')
        ax.set_ylabel('True')
        
        thresh = cm_percent.max() / 2.0
        for i in range(cm_percent.shape[0]):
            for j in range(cm_percent.shape[1]):
                ax.text(j, i, f'{round(cm_percent[i, j], 1)}%\n({cm_absolut[i, j]})',
                        ha="center", va="center",
                        color="black" if cm_percent[i, j] > thresh else "white")
        
    for i, model in enumerate(models):
        y_pred = model.predict(X_nn, verbose=0)
        cm_absolut = confusion_matrix(np.argmax(y_nn, axis=1), np.argmax(y_pred, axis=1))
        cm_percent = cm_absolut / cm_absolut.sum(axis=1, keepdims=True) * 100
        
        ax = axes[1, i]
        ax.imshow(cm_percent, interpolation='nearest', cmap='viridis')
        ax.set_title(model_names[i])
        ax.set_xticks(np.arange(len(class_names)))
        ax.set_yticks(np.arange(len(class_names)))
        ax.set_xticklabels(class_names)
        ax.set_yticklabels(class_names)
        ax.set_xlabel('Predicted')
        ax.set_ylabel('True')
        
        thresh = cm_percent.max() / 2.0
        for i in range(cm_percent.shape[0]):
            for j in range(cm_percent.shape[1]):
                ax.text(j, i, f'{round(cm_percent[i, j], 1)}%\n({cm_absolut[i, j]})',
                        ha="center", va="center",
                        color="black" if cm_percent[i, j] > thresh else "white")

    plt.tight_layout()
    plt.show()

def plot_custom_model_confusion_matrix(X_nn, y_nn):

    model = load_model('model_custom')

    class_names = ['negative', 'neutral', 'positive']

    plt.figure(figsize=(10, 4))
        
    y_pred = model.predict(X_nn, verbose=0)
    cm_absolut = confusion_matrix(np.argmax(y_nn, axis=1), np.argmax(y_pred, axis=1))
    cm_percent = cm_absolut / cm_absolut.sum(axis=1, keepdims=True) * 100
    
    plt.imshow(cm, interpolation='nearest', cmap='viridis')
    plt.title('model_custom')
    tick_positions = np.arange(len(class_names))
    plt.xticks(tick_positions, class_names, rotation=90)
    plt.yticks(tick_positions, class_names)
    plt.xlabel('Predicted')
    plt.ylabel('True')
    
    thresh = cm.max() / 2.0
    for i in range(cm_percent.shape[0]):
            for j in range(cm_percent.shape[1]):
                plt.text(j, i, f'{round(cm_percent[i, j], 1)}%\n({cm_absolut[i, j]})',
                        ha="center", va="center",
                        color="black" if cm_percent[i, j] > thresh else "white")
                
    plt.show()