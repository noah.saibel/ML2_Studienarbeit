# ML2 Studienarbeit 

## Virtuelle Umgebung erstellen

Für Windows
```bash
python -m venv venv_name
venv_name\Scripts\activate
```

Für Linux/Mac
```bash
python3 -m venv venv_name
source venv_name/bin/activate
```

## Importieren der Projektdaten
zip-Archiv in Ordner venv Ordner entpacken

## Installation der Bibliotheken

```pip install -r requirements.txt```

## Trainieren neuer Modelle
Das Trainieren neuer Modelle ist standardmäßig deaktiviert. Modelle werden aus den Ordnern `/classifier` und `models` geladen um die Laufzeit zu verringern.

Um neue neuronale Netze zu Trainieren muss im Abschnitt 5.1 `train_new_nn_model` auf True gesetzt werden. Um eins der drei neuronalen Netze `model_1`, `model_2` oder `model_3` neu zu trainieren muss als `model_name` der jeweilige Modellname angegeben werden.

Um die klassischen Modelle neu zu trainieren muss in 5.2 `train_new_classic_models` auf True gesetzt werden.